import Foundation
import RxSwift

enum ApiError: Error {
    case apiX, apiY, apiZ
}

extension ApiError: LocalizedError {
    var errorDescription: String? {
        switch self {
        case .apiX:
            return "ApiXでエラーが起きました"
        case .apiY:
            return "ApiYでエラーが起きました"
        case .apiZ:
            return "ApiZでエラーが起きました"
        }
    }
}

struct DummyResponse: Decodable {
    let code: Int
    let detail: String
}

func fetchResponse(resourceName: String) -> DummyResponse {
    // json読み込んで結果を返すのみ
    guard let path = Bundle.main.path(forResource: resourceName, ofType: "json") else {
        return DummyResponse(code: 500, detail: "Resource not found.")
    }
    
    do {
        let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
        return try JSONDecoder().decode(DummyResponse.self, from: data)
    } catch {
        return DummyResponse(code: 500, detail: "JSON Decode Error")
    }
}

func callApiX() -> Observable<(String ,String)> {
    return Observable.create({ observer in
        let response = fetchResponse(resourceName: "responseX")
        print("ApiX result -> code: \(response.code), detail: \(response.detail)")
        if response.code == 200 {
            observer.on(.next((response.detail, "keyForApiY")))
        } else {
            observer.on(.error(ApiError.apiX))
        }
        
        return Disposables.create {
            print("ApiX -> Disposed")
        }
    })
}

func callApiY(key: String) -> Observable<(String)> {
    print("key: \(key)")
    return Observable.create({ observer in
        let response = fetchResponse(resourceName: "responseY")
        print("ApiY result -> code: \(response.code), detail: \(response.detail)")
        if response.code == 200 {
            observer.on(.next(("keyForApiZ")))
        } else {
            observer.on(.error(ApiError.apiY))
        }
        return Disposables.create {
            print("ApiY -> Disposed")
        }
    })
}

func callApiZ(key: String) -> Observable<String> {
    print("key: \(key)")
    return Observable.create({ observer in
        let response = fetchResponse(resourceName: "responseZ")
        print("ApiZ result -> code: \(response.code), detail: \(response.detail)")
        if response.code == 200 {
            observer.on(.next(response.detail))
        } else {
            observer.on(.error(ApiError.apiZ))
        }
        return Disposables.create {
            print("ApiZ -> Disposed")
        }
    })
}

func main() {
    var responseX: String = ""

    // ApiXをコール -> responseはObservable<(A,B)>
    callApiX()
        .flatMap { (a, b) -> Observable<String> in
            // (A)を保持
            responseX = a
            // (B)を渡してApiYをコール -> responseはObservable<(C)>
            return callApiY(key: b)
        }
        .flatMap {
            // (C)を渡してApiZをコール -> responseはObservable<(D)>
            return callApiZ(key: $0)
        }
        .map {
            // mapで変換して Observable<(A,D)> を返す
            return (responseX, $0)
        }
        .subscribe(
            onNext: {
                print("All API Passed.")
                print("result -> A:\($0.0), D:\($0.1)")
            },
            onError: { error in
                print("error: \(error.localizedDescription)")
            })
        .disposed(by: DisposeBag())
}

main()
